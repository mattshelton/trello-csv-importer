# Trello CSV Importer

## Description

This class imports cards onto a Trello board from a CSV file file in the following format:

  - Name (name of card)
  - Description (description of card)
  - List (target Trello list)
  - Labels (pipe-separated, e.g. "one|two")

This Ruby script is designed to be run using `foreman run ./trello-cards-from-csv.rb [filename]`, as it uses a number of environment variables from a file named '.env'.

This script requires Ruby 2.2+ in order to support the `ruby-trello` gem.

## Setup ##

1. Crete a Trello account and a board with as many lists as you require for your import.
2. Set up Ruby, bundler, etc.
3. Clone this repository.
4. Create a `.env` file using `.env.example` as your guide. You will need to start by visiting https://trello.com/1/appKey/generate to generate your application ID and secret, then adding your app ID to another URL to generate your user token with the proper permissions.
5. Execute via `foreman run ./trello-cards-from-csv.rb [filename]` or pass in the environment variables before running the script like so:
   `TRELLO_APP_ID=xxx TRELLO_APP_SECRET=xxx TRELLO_USER_TOKEN=xxx BOARD_NAME="xxx" MEMBER_NAME="xxx" ./trello-cards-from-csv.rb[filename]`

## Credits

This is a freshening of the script written by [Josh McArthur](https://joshmcarthur.com/2012/05/31/import-trello-cards-from-a-csv-file.html) for his use at [3months](http://www.3months.com). The original source can be found [here](https://gist.github.com/joshmcarthur/2839352#file-trello-cards-from-csv-rb).