#!/usr/bin/env ruby

# You can skip this bit if you wish - you will need the 'ruby-trello' gem installed, and
# optionally, 'foreman' to run the script with.

require 'bundler/setup'
require 'trello'
require 'csv'

# TrelloCardsFromCSV
# This class imports cards onto a Trello board from a CSV file file in the following format:
#   - Name (name of card)
#   - Description (description of card)
#   - List (target Trello list)
#   - Labels (pipe-separated, e.g. "one|two")
#
# This Ruby script is designed to be run using `foreman run trello-cards-from-csv.rb [filename]`, as it
# uses a number of environment variables from a file named '.env'.
# This file should contain each of the variables below on a new line, but you'll only need to enter them once.
# Alternatively you can set up the variables you need before running the script, like so:
# TRELLO_APP_ID=xxx TRELLO_APP_SECRET=xxx TRELLO_USER_TOKEN=xxx BOARD_NAME="xxx" MEMBER_NAME="xxx" ./trello-cards-from-csv.rb
#
# Here's what each of these variables are:
# TRELLO_APP_ID, TRELLO_APP_SECRET - The Trello application details generated from: https://trello.com/1/appKey/generate
# TRELLO_USER_TOKEN - The token you get by visiting:
#   https://trello.com/1/connect?key=[YOUR TRELLO APP ID]&name=MyApp&response_type=token&scope=read,write,account&expiration=never
#   in your browser and granting permissions.
# BOARD_NAME - the name of the board to add cards to (be sure to quote)
# MEMBER_NAME - the name of the member to authenticate as
#
# See methods below for more documentation.

class TrelloCardsFromCSV

  # Include Trello classes so that we can use them directly
  include Trello
  include Trello::Authorization

  attr_accessor :filename, :board_name, :member, :member_name, :board, :validLabels, :validLists

  # Initialize the object and process the card file
  #
  # filename - the relative name of the file to read card data from (must be well-formed CSV)
  # board_name - the name of the board to add cards to (comes from an environment variable)
  # member_name - the name of the member to add cards as (comes from an environment variable)
  #
  def initialize(filename, board_name, member_name)
    self.filename = filename
    self.board_name = board_name
    self.member_name = member_name

    # Set up the Trello authorization. This uses a number of environment variables
    # directly to set up the authorization with Trello.
    Trello::Authorization.const_set :AuthPolicy, OAuthPolicy
    OAuthPolicy.consumer_credential = OAuthCredential.new ENV['TRELLO_APP_ID'], ENV['TRELLO_APP_SECRET']
    OAuthPolicy.token = OAuthCredential.new ENV['TRELLO_USER_TOKEN'], nil

    # Find the member to use when looking for boards
    self.member = Member.find(self.member_name)

    # Find the board by it's name
    self.board = self.member.boards.select { |b| b.name == self.board_name }.first

    # Stop running the script if a matching board could not be found
    raise "No board found" unless board

    puts "Using board #{board.name}"

    # Handle board labels
    self.validLabels = {}

    self.board.labels.each { |l|
        self.validLabels[l.name] = l.id
    }

    # Grab the board's lists, using the first one as our default
    the_lists = self.board.lists
    listId = the_lists.first.id
    puts "Using list #{the_lists.first.name} by default."

    self.validLists = {}
    the_lists.each { |list|
        self.validLists[list.name] = list.id
    }

    # Open file and import
    CSV.foreach(filename, 'r:bom|utf-8') do |row|

        # Field 0 - The name of the card. This field cannot be empty.
        the_name = row[0]
        if the_name.to_s.empty?
            puts "ERROR: Name empty. Skipping row."
            next
        end

        # Field 1 - The card description. May be empty.
        the_desc = row[1]

        # Field 2 - The destination list. May be empty/invalid, and if so will use the default (first) list.
        listString = row[2]
        unless listString.to_s.empty?
            if self.validLists.key?(listString)
                my_list_id = self.validLists[listString]
            else
                my_list_id = listId
                puts "WARNING: List #{listString} is invalid. Using default instead."
            end
        end

        # Field 3 - A list of labels, pipe-delimited.
        # If they are valid, add them to the card. If not, skip them.
        labelString = row[3]
        unless labelString.to_s.empty?
            add_labels = []
            labelSet = labelString.split("|")
            labelSet.each { |l|
                if self.validLabels.key?(l)
                    add_labels.push(self.validLabels[l])
                else
                    puts "WARNING: Skipped invalid label #{l}"
                end
            }
        end

      # Create the card using the specified name (0), description (1), list (2), and [optional] labels (3)
      card = Card.create({
        :list_id => my_list_id,
        :name => the_name,
        :desc => the_desc,
        :card_labels => add_labels,
        :pos => "bottom"
      })
      puts "Imported #{card.name}."
    end
  end
end

# Set up and run the script, passing in environment variables except for the filename, which is passed in from the command.
TrelloCardsFromCSV.new(
  ARGV.first,
  ENV['BOARD_NAME'],
  ENV['MEMBER_NAME']
)